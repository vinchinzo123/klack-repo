import express from "express";
import mongoose from "mongoose";
import querystring from "querystring";
import { MessageModel } from "./messageModel";
const port = 3000;
const app = express();

// List of all messages
let messages = [];

// Track last active times for each sender
let users = {};

app.use(express.static("./public"));
app.use(express.json());

mongoose
  .connect("mongodb://localhost/rsvp", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log("joined db"))
  .catch((err) => console.log(err));

// generic comparison function for case-insensitive alphabetic sorting on the name field
function userSortFn(a, b) {
  var nameA = a.name.toUpperCase(); // ignore upper and lowercase
  var nameB = b.name.toUpperCase(); // ignore upper and lowercase
  if (nameA < nameB) {
    return -1;
  }
  if (nameA > nameB) {
    return 1;
  }
  // names must be equal
  return 0;
}

app.get("/messages", async (request, response) => {
  const now = Date.now();

  const requireActiveSince = now - 15 * 1000;

  const messageList = await MessageModel.find((err, mes) => {
    if (err) return console.error(err);
  });

  let usersSimple = Object.keys(users).map((x) => ({
    name: x,
    active: users[x] > requireActiveSince,
  }));
  usersSimple.sort(userSortFn);
  usersSimple.filter((a) => a.name !== request.query.for);
  users[request.query.for] = now;
  response.send({ messages: messageList, users: usersSimple });
});

app.post("/messages", async (request, response) => {
  // add a timestamp to each incoming message.
  const timestamp = Date.now();
  request.body.timestamp = timestamp;

  // append the new message to the message list
  const message = new MessageModel(request.body);

  message.save((err, mes) => {
    if (err) return console.error(err);
    console.log(mes);
  });
  // update the posting user's last access timestamp (so we know they are active)
  users[request.body.sender] = timestamp;

  // Send back the successful response.
  response.status(201);
  response.send(request.body);
});

app.delete("/messages", async (req, res) => {
  await MessageModel.deleteMany({}, (err, mes) => {
    if (err) return console.error(err);
    console.log(mes);
  });
  res.status(200);
  res.send("deleted!");
});

app.listen(3000, () => {
  console.log(`Listening on port http://localhost:${port}`);
});
