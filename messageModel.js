import mongoose from "mongoose";

const messageSchema = new mongoose.Schema(
  {
    sender: String,
    message: String,
    timestamp: Number,
  }
  // ,
  // {
  //   timestamps: true,
  // }
);

export const MessageModel = mongoose.model("Response", messageSchema);
